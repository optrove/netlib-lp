# Netlib-LP

The NETLIB linear programming problems (http://www.netlib.org/lp/data/) in Standard Input Format (SIF), see http://www.numerical.rl.ac.uk/lancelot/sif/sifhtml.html.

The problems are all in MPS format, which is itself a subset of SIF. The problems have been classified according to the CUTE classification system (see, Bongartz, Conn, Gould and Toint, ACM Transactions on Mathematical Software 21, 1995, pp.  123-160), and have been slightly reformatted for consistency.
